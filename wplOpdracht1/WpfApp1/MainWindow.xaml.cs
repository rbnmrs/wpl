﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void img04_MouseEnter(object sender, MouseEventArgs e)
        {
           Background = Brushes.Orange;
        }

        private void img04_MouseLeave(object sender, MouseEventArgs e)
        {
            Background = Brushes.LightGray;
        }
        private void img01_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = Brushes.Red;
        }

        private void img01_MouseLeave(object sender, MouseEventArgs e)
        {
            Background = Brushes.LightGray;
        }
        private void img02_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = Brushes.Yellow;
        }

        private void img02_MouseLeave(object sender, MouseEventArgs e)
        {
            Background = Brushes.LightGray;
        }
        private void img03_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = Brushes.Purple;
        }

        private void img03_MouseLeave(object sender, MouseEventArgs e)
        {
            Background = Brushes.LightGray;
        }
    }
}
